<div class="product-variations">
    <h3>Color Variations</h3>
    <div class="row">
        <?php
        $familysku = get_post_meta($post->ID, 'style', true);
        if(is_singular( 'laminate' )){
            $flooringtype = 'laminate';
        } elseif(is_singular( 'hardwood' )){
            $flooringtype = 'hardwood';
        } elseif(is_singular( 'carpeting' )){
            $flooringtype = 'carpeting';
        } elseif(is_singular( 'luxury_vinyl' )){
            $flooringtype = 'luxury_vinyl';
        } elseif(is_singular( 'vinyl' )){
            $flooringtype = 'vinyl';
        } elseif(is_singular( 'solid_wpc_waterproof' )){
            $flooringtype = 'solid_wpc_waterproof';
        } elseif(is_singular( 'tile' )){
            $flooringtype = 'tile';
        }

        $args = array(
            'post_type'      => $flooringtype,
            'posts_per_page' => -1,
            'post_status'    => 'publish',
            'meta_query'     => array(
                array(
                    'key'     => 'style',
                    'value'   => $familysku,
                    'compare' => '='
                )
            )
        );
        ?>
        <?php
        $the_query = new WP_Query( $args );
        ?>


        <div class="fr-slider color_variations_slider" data-fr='{"slidesToScroll":7,"slidesToShow":7,"arrows":false,"infinite": false}'>
            <a href="#" class="arrow prev"><i class="fa fa-angle-left"></i></a>
            <a href="#" class="arrow next"><i class="fa fa-angle-right"></i></a>
            <div class="slides">
                <?php  while ( $the_query->have_posts() ) {
                $the_query->the_post(); ?>
                    <div class="slide col-md-2 col-sm-3 col-xs-6 color-box text-center">
                        <a  href="<?php the_permalink(); ?>">
                         		<?php
									$item = get_field('sku');
									$itemImage = explode("_", $item);	
									$imageNew= $itemImage[1] .'_'. $itemImage[0];
									$swatch = 0;
									$main = 0;
									$style = "";
	
									//Code to check the image exits.	
									$url ='https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_SWATCH?req=exists,text';
									$response = wp_remote_post($url, array('method' => 'GET'));		
									$ImageExist = $response["body"];	
									$exits = strpos($ImageExist,"catalogRecord.exists=0");		
									if ($exits === false) {	
											$swatch =1;
									}
									if($swatch !=1) {
										$url ='https://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_MAIN?req=exists,text';
										$response = wp_remote_post($url, array('method' => 'GET'));		
										$ImageExist = $response["body"];	
										$exits = strpos($ImageExist,"catalogRecord.exists=0");		
										if ($exits === false) {	
											$main = 1;
										} 								
									}
	
										if($flooringtype == "carpeting" && $swatch == 1) {
											$image = 'http://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_SWATCH?fmt=pjpeg&fit=crop&wid=222&hei=222';
										} elseif($main == 1) {
// 											$itemImage = get_field('swatch_image_link');
// 											$image= $itemImage . "?fmt=jpg&qlt=60&hei=160&wid=160&fit=crop,0";
												$image = 'http://shawfloors.scene7.com/is/image/ShawIndustries/'.$imageNew.'_MAIN?fmt=pjpeg&fit=crop&wid=222&hei=222';
												$style= "padding: 5px;";
										} else {
												$image = 'http://shawfloors.scene7.com/is/image/ShawIndustriesRender/'.$imageNew.'_MAIN?fmt=pjpeg&fit=crop&wid=222&hei=222';
												$style= "padding: 5px;";
										}
									
								?>
							
                                <img src="<?php echo $image; ?>" style="<?php echo $style; ?>" class="swatch-img tooltipped" data-position="top" data-delay="50" data-tooltip="<?php the_title(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" width="100" height="100" />
                         



                        </a>
                        <br />
                        <small style="<?php echo $style; ?>"><?php the_field('color'); ?></small>
                    </div>
                <?php } ?>
            </div>
        </div>

    <?php wp_reset_postdata(); ?>


</div>
    </div>