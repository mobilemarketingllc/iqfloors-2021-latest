<?php get_header(); ?>
<!-- <div class="container">
    <div class="row">
        <div class="col-md-12">
    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			 <a href="/">Home</a> &raquo; <a href="/flooring/">Flooring</a> &raquo; <a href="/flooring/laminate/">Laminate</a> &raquo; <a href="/flooring/laminate/products/">Laminate Products</a> &raquo; <?php the_title(); ?>
    
    </div>
        </div>
    </div>
</div> -->

<div class="container">
    <div class="row">
        <div class="fl-content product<?php //FLTheme::content_class(); ?>">
            <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                <?php get_template_part('content', 'single-product'); ?>
            <?php endwhile; endif; ?>
        </div>
        <?php //FLTheme::sidebar('right'); ?>
    </div>
</div>
<?php get_footer(); ?>