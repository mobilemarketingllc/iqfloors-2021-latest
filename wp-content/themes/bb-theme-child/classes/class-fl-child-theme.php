<?php

/**
 * Helper class for child theme functions.
 *
 * @class FLChildTheme
 */
final class FLChildTheme {
    
    /**
	 * Enqueues scripts and styles.
	 *
     * @return void
     */
    static public function enqueue_scripts()
    {
	    wp_enqueue_style( 'fl-child-theme', FL_CHILD_THEME_URL . '/style.css' );
    }
	
	static public function child_pagination($query) 
	{
		$total_pages = $query->max_num_pages;
		$permalink_structure = get_option('permalink_structure');
		$paged = is_front_page() ? get_query_var('page') : get_query_var('paged');
		
		if($total_pages > 1) {
		
			if(!$current_page = $paged) {
				$current_page = 1;
			}
		
			if(empty($permalink_structure)) {
				$format = '&paged=%#%';
			} 
			else {
				$format = 'page/%#%/';
			}
			
			return paginate_links(array(
				'base'	   => get_pagenum_link(1) . '%_%',
				'format'   => $format,
				'current'  => $current_page,
				'total'	   => $total_pages,
				'prev_text' => __('prev'),
				'next_text' => __('next'),
				'type'	   => 'array'
			));
		}
	} 
	static public function archive_nav() {
		global $wp_query;

		if ( function_exists( 'wp_pagenavi' ) ) {
			wp_pagenavi();
		} elseif ( $wp_query->max_num_pages > 1 ) {
			echo '<nav class="fl-archive-nav clearfix">';
			echo '<div class="fl-archive-nav-prev">' . get_previous_posts_link( __( '&laquo; LOAD PREVIOUS', 'fl-automator' ) ) . '</div>';
			echo '<div class="fl-archive-nav-next">' . get_next_posts_link( __( 'MORE RESULTS &raquo;', 'fl-automator' ) ) . '</div>';
			echo '</nav>';
		}
	}
}