<?php get_header(); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
       <a href="<?php echo site_url(); ?>/flooring/carpet/products/">Carpet Products</a> &raquo;
        <?php the_title(); ?>
    </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="fl-content product<?php //FLTheme::content_class(); ?>">
            <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                <?php get_template_part('content', 'single-product'); ?>
            <?php endwhile; endif; ?>
        </div>
        <?php //FLTheme::sidebar('right'); ?>
    </div>
</div>
<?php get_footer(); ?>